import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from './components/administration/users/users.component';
import { NumberSeqComponent } from './components/administration/number-seq/number-seq.component';
import { CustomerComponent } from './components/business-partner/customer/customer.component';
import { MerchantComponent } from './components/business-partner/merchant/merchant.component';
import { CourierComponent } from './components/business-partner/courier/courier.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'administration/users', component: UsersComponent },
  { path: 'administration/number-seq', component: NumberSeqComponent },
  { path: 'business-partner/customer', component: CustomerComponent },
  { path: 'business-partner/merchant', component: MerchantComponent },
  { path: 'business-partner/courier', component: CourierComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
