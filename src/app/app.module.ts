import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppheaderComponent } from './components/layout/appheader/appheader.component';
import { AppmenuComponent } from './components/layout/appmenu/appmenu.component';
import { AppfooterComponent } from './components/layout/appfooter/appfooter.component';
import { AppsettingsComponent } from './components/layout/appsettings/appsettings.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from './components/administration/users/users.component';
import { NumberSeqComponent } from './components/administration/number-seq/number-seq.component';
import { CustomerComponent } from './components/business-partner/customer/customer.component';
import { MerchantComponent } from './components/business-partner/merchant/merchant.component';
import { CourierComponent } from './components/business-partner/courier/courier.component';
import { CoaComponent } from './components/accounting/coa/coa.component';
import { JournalEntryComponent } from './components/accounting/journal-entry/journal-entry.component';
import { Report1Component } from './components/accounting/report/report1/report1.component';
import { Report2Component } from './components/accounting/report/report2/report2.component';
import { Setup1Component } from './components/accounting/setup/setup1/setup1.component';
import { Setup2Component } from './components/accounting/setup/setup2/setup2.component';
import { CashBankComponent } from './components/cash-bank/cash-bank/cash-bank.component';

@NgModule({
  declarations: [
    AppComponent,
    AppheaderComponent,
    AppmenuComponent,
    AppfooterComponent,
    AppsettingsComponent,
    DashboardComponent,
    UsersComponent,
    NumberSeqComponent,
    CustomerComponent,
    MerchantComponent,
    CourierComponent,
    CoaComponent,
    JournalEntryComponent,
    Report1Component,
    Report2Component,
    Setup1Component,
    Setup2Component,
    CashBankComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
